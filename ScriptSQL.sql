--BASE DE DATOS
create database PachinDB
	go
use PachinDB



--TABLA EMPLEADO
--Cada empleado tiene un rol: 1 es admin, 2 pedidos y 3 caja
--El nombre de usuario nunca se puede cambiar, es permanente
set ansi_nulls on
go
set quoted_identifier on
go
create table Empleado
(
	Legajo int identity (1,1) primary key,
	Nombre varchar(50),
	Apellido varchar(50),
	DNI varchar(15),
	Fecha date,
	Telefono varchar(15),
	Direccion varchar(100),
	Usuario varchar(15),
	Clave varchar(128),
	Rol int
)



--Alta de usuario administrador
--Usuario: admin
--Clave: admin (encriptada SHA512)
set ansi_nulls on
go
set quoted_identifier on
go
insert into Empleado(Usuario, Clave, Rol) values ('admin', 'C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC', 1)



--Agregar empleado
set ansi_nulls on
go
set quoted_identifier on
go
create procedure AgregarEmpleado
	@Nombre varchar(50),
	@Apellido varchar(50),
	@DNI varchar(15),
	@Fecha date,
	@Telefono varchar(15),
	@Direccion varchar(100),
	@Usuario varchar(15),
	@Clave varchar(128),
	@Rol int
as
begin
	set nocount off;
	insert into Empleado (Nombre, Apellido, DNI, Fecha, Telefono, Direccion, Usuario, Clave, Rol) values (@Nombre, @Apellido, @DNI, @Fecha, @Telefono, @Direccion, @Usuario, @Clave, @Rol)
end



--Ver empleados
set ansi_nulls on
go
set quoted_identifier on
go
create procedure MostrarEmpleados
as
	select * from Empleado
go



--Editar empleado
--Se permite editar todos los datos, por si hay un error de tipeo
--El usuario no se puede cambiar y la clave solo puede modificarla el admin
--Dado que para este caso se hizo borrado fisico para Epleado, el cambio de la clave
--puede resultar util por ejemplo al momento de suspender a un empleado
set ansi_nulls on
go
set quoted_identifier on
go
create procedure EditarEmpleado
	@Legajo int,
	@Nombre varchar(50),
	@Apellido varchar(50),
	@DNI varchar(15),
	@Fecha date,
	@Telefono varchar(15),
	@Direccion varchar(100),
	@Clave varchar(128),
	@Rol int
as
	update Empleado set Nombre=@Nombre, Apellido=@Apellido, DNI=@DNI, Fecha=@Fecha, Telefono=@Telefono, Direccion=@Direccion, Clave=@Clave, Rol=@Rol where Legajo=@Legajo
go



--Eliminar empleado
--El borrado es fisico
set ansi_nulls on
go
set quoted_identifier on
go
create procedure EliminarEmpleado
	@Legajo int
as
	delete from Empleado where Legajo=@Legajo
go



--TABLA PEDIDO
--El pedido tiene los siguientes datos:
--Cliente: Nombre, apellido
--Comanda: cantidad de panchos y hamburguesas, precio
--Estado: 0 es pendiente y 1 entregado
set ansi_nulls on
go
set quoted_identifier on
go
create table Pedido
(
	NroPedido int identity (1,1) primary key,
	NombreCliente varchar(100),
	CantidadPanchos int,
	CantidadHamburguesas int,
	Precio float,
	Estado int
)



--Agregar pedido
set ansi_nulls on
go
set quoted_identifier on
go
create procedure AgregarPedido
	@NombreCliente varchar(100),
	@CantidadPanchos int,
	@CantidadHamburguesas int,
	@Precio float,
	@Estado int
as
begin
	set nocount off;
	insert into Pedido(NombreCliente, CantidadPanchos, CantidadHamburguesas, Precio, Estado) values (@NombreCliente, @CantidadPanchos, @CantidadHamburguesas, @Precio, @Estado)
end



--Ver pedidos 
--Ambos roles de empleados solo ven pedidos sin entregar
set ansi_nulls on
go
set quoted_identifier on
go
create procedure MostrarPedidos
as
	select * from Pedido where Estado=0
go



--Ver pedidos 
--Modo administrador, donde ve todos los pedidos entregados (ventas efectivas)
set ansi_nulls on
go
set quoted_identifier on
go
create procedure ListadoVentas
as
	select * from Pedido where Estado=1 and Precio>0
go



--Editar pedido
--Edicion rol Cajero
set ansi_nulls on
go
set quoted_identifier on
go
create procedure EditarPedidoCajero
	@NroPedido int,
	@NombreCliente varchar(100),
	@CantidadPanchos int,
	@CantidadHamburguesas int,
	@Precio float
as
	update Pedido set NombreCliente=@NombreCliente, CantidadPanchos=@CantidadPanchos, CantidadHamburguesas=@CantidadHamburguesas, Precio=@Precio where NroPedido=@NroPedido
go



--Cancelar pedido
--Edicion rol Cajero
--El pedido queda en los registros en CERO, no se borra fisicamente
set ansi_nulls on
go
set quoted_identifier on
go
create procedure CancelarPedido
	@NroPedido int,
	@NombreCliente varchar(100)
as
	update Pedido set NombreCliente=@NombreCliente, CantidadPanchos=0, CantidadHamburguesas=0, Precio=0, Estado=1 where NroPedido=@NroPedido
go



--Editar pedido
--Edicion rol Cocina (solo entrega el pedido)
set ansi_nulls on
go
set quoted_identifier on
go
create procedure EditarPedidoCocina
	@NroPedido int
as
	update Pedido set Estado=1 where NroPedido=@NroPedido
go



--Buscar pedido por nro de pedido o nombre del cliente
set ansi_nulls on
go
set quoted_identifier on
go
create proc BuscarPedido
	@PalabraClave varchar(100)
as
	select * from Pedido where NombreCliente=@PalabraClave and Estado=0
go