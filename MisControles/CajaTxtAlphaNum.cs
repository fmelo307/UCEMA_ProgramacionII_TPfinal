﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace MisControles
{
    public class CajaTxtAlphaNum : TextBox
    {
        // Para el usuario y clave solo se permiten caracteres alfanumericos
        public void ValidarTexto() 
        {
            int tmp = Text.Length;
            Text = string.Concat(Text.Where(char.IsLetterOrDigit));
            if (Text.Length != tmp) 
            {
                string msg = "Entrada no válida en " + Name;
                MessageBox.Show("No se permiten caracteres especiales.\nSolo caracteres alfanuméricos", msg);
            }
        }
    }
}
