﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Data;


namespace Funciones
{
    public static class Encriptado
    {
        //Recibe string y devuelve string luego de hash
        public static string GenerarSHA512String(string dato) 
        {
            SHA512 sha512 = SHA512Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(dato);
            byte[] hash = sha512.ComputeHash(bytes);
            return HashDevolverString(hash);
        }




        // Devuelve el hash como string
        private static string HashDevolverString(byte[] hash)
        {
            StringBuilder rta = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                rta.Append(hash[i].ToString("X2"));
            }
            return rta.ToString();
        }
    }
}
