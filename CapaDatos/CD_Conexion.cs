﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CapaDatos
{
    // Clase PUBLICA
    public class CD_Conexion
    {
        // Recordar que la barra invertida va DOBLE! Sino no funciona
        private SqlConnection Conexion = new SqlConnection("Data Source=FERNANDOMEL57BB\\SQLEXPRESS;Initial Catalog=PachinDB;Integrated Security=True");




        // Metodo para abrir la conexion
        public SqlConnection AbrirConexion()
        {
            if (Conexion.State == ConnectionState.Closed)
                Conexion.Open();
            return Conexion;
        }




        // Metodo para cerrar la conexion
        public SqlConnection CerrarConexion()
        {
            if (Conexion.State == ConnectionState.Open)
                Conexion.Close();
            return Conexion;
        }
    }
}