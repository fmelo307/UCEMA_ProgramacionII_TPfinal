﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Agregar Librerias
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;


namespace CapaDatos
{
    // Clase PUBLICA
    public class CD_Empleado
    {
        // Instancio la conexion con la base de datos
        private CD_Conexion conexion = new CD_Conexion();




        // Declaracion del datareader, tabla de la base y comando SQL
        SqlDataReader leer; // lee una fila
        DataTable tabla = new DataTable();
        SqlCommand comando = new SqlCommand();




        // Metodo Mostrar
        public DataTable Mostrar()
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "MostrarEmpleados";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }




        // Metodo Agregar
        public void Agregar(string nombre, string apellido, string dni, DateTime fecha, string telefono, string direccion, string usuario, string clave, int rol)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "AgregarEmpleado";
            comando.CommandType = CommandType.StoredProcedure;
            #region "parameters"
            comando.Parameters.AddWithValue("@Nombre", nombre);
            comando.Parameters.AddWithValue("@Apellido", apellido);
            comando.Parameters.AddWithValue("@DNI", dni);
            comando.Parameters.AddWithValue("@Fecha", fecha);
            comando.Parameters.AddWithValue("@Telefono", telefono);
            comando.Parameters.AddWithValue("@Direccion", direccion);
            comando.Parameters.AddWithValue("@Usuario", usuario);
            comando.Parameters.AddWithValue("@Clave", clave);
            comando.Parameters.AddWithValue("@Rol", rol);
            #endregion
            comando.ExecuteNonQuery();
            comando.Parameters.Clear();
            conexion.CerrarConexion();
        }




        // Metodo Editar
        // El admin puede editar todos los datos, excepto el usuario y el legajo
        public void Editar(string nombre, string apellido, string dni, DateTime fecha, string telefono, string direccion, string clave, int rol, int legajo)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "EditarEmpleado";
            comando.CommandType = CommandType.StoredProcedure;
            #region "parameters"
            comando.Parameters.AddWithValue("@Nombre", nombre);
            comando.Parameters.AddWithValue("@Apellido", apellido);
            comando.Parameters.AddWithValue("@DNI", dni);
            comando.Parameters.AddWithValue("@Fecha", fecha);
            comando.Parameters.AddWithValue("@Telefono", telefono);
            comando.Parameters.AddWithValue("@Direccion", direccion);
            comando.Parameters.AddWithValue("@Clave", clave);
            comando.Parameters.AddWithValue("@Rol", rol);
            comando.Parameters.AddWithValue("@Legajo", legajo);
            #endregion
            comando.ExecuteNonQuery();
            comando.Parameters.Clear();
            conexion.CerrarConexion();
        }




        // Metodo Eliminar
        // El borado es fisico
        // Se uso borrado fisico para Empleado y logico para Pedido (para usar ambos metodos vistos)
        public void Eliminar(int legajo)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "EliminarEmpleado";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@Legajo", legajo);
            comando.ExecuteNonQuery();
            comando.Parameters.Clear();
            conexion.CerrarConexion();
        }
    }
}
