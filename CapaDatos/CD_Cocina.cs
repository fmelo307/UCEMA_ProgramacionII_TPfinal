﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Agregar Librerias
using System.Data.SqlClient;
using System.Data;


namespace CapaDatos
{
    // Clase PUBLICA
    public class CD_Cocina
    {
        // Instancio la conexion con la base de datos
        private CD_Conexion conexion = new CD_Conexion();




        // Declaracion del datareader, tabla de la base y comando SQL
        SqlDataReader leer; // lee una fila
        DataTable tabla = new DataTable();
        SqlCommand comando = new SqlCommand();




        // Metodo Mostrar
        public DataTable Mostrar()
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "MostrarPedidos";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }




        // Metodo Entregar
        // El cocinero solo puede editar el pedido de pendiente a entregado
        public void Entregar(int nroPedido)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "EditarPedidoCocina";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@NroPedido", nroPedido);
            comando.ExecuteNonQuery();
            comando.Parameters.Clear();
            conexion.CerrarConexion();
        }




        // Metodo Buscar
        public DataTable Buscar(string busqueda)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "BuscarPedido";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@PalabraClave", busqueda);
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }
    }
}