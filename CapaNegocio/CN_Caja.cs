﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Agregar Librerias
using System.Data;
using System.Data.SqlClient;
using CapaDatos;


namespace CapaNegocio
{
    // Clase PUBLICA
    public class CN_Caja
    {
        // Instancio la clase de CAPADATOS
        private CD_Caja objetoCD = new CD_Caja();




        // Metodo que muestra el listado de pedidos sin entregar
        public DataTable MostrarPedidos()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.Mostrar();
            return tabla;
        }




        // Metodo que muestra el listado de ventas realizadas
        // Solo para uso del ADMIN
        public DataTable ListarVentas()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.MostrarVentas();
            return tabla;
        }




        // Metodo para agregar un nuevo pedido
        public void AgregarPedido(string nombreCliente, int cantidadPanchos, int cantidadHamburguesas, float precio)
        {
            objetoCD.Agregar(nombreCliente, cantidadPanchos, cantidadHamburguesas, precio);
        }




        // Metodo para modificar un pedido seleccionado
        public void ModificarPedido(string nombreCliente, int cantidadPanchos, int cantidadHamburguesas, float precio, string nroPedido)
        {
            objetoCD.Editar(nombreCliente, cantidadPanchos, cantidadHamburguesas, precio, Convert.ToInt32(nroPedido));
        }




        // Metodo para cancelar un pedido por numero de pedido
        public void CancelarPedido(string nombreCliente, string nroPedido)
        {
            objetoCD.Cancelar(nombreCliente, Convert.ToInt32(nroPedido));
        }




        // Metodo para mostrar en el datagrid un pedido
        // La busqueda se puede hacer por numero de pedido o nombre del cliente
        public DataTable BuscarPedido(string busqueda)
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.Buscar(busqueda);
            return tabla;
        }
    }
}
