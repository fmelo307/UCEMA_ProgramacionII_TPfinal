﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Agregar Librerias
using System.Data;
using System.Data.SqlClient;
using CapaDatos;
using Funciones;


namespace CapaNegocio
{
    // Clase PUBLICA
    public class CN_Empleado
    {
        // Instancio la clase de CAPADATOS
        private CD_Empleado objetoCD = new CD_Empleado();




        // Metodo que muestra el listado de empleados en el datagrid
        public DataTable MostrarEmpleados()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.Mostrar();
            return tabla;
        }




        // Metodo para agregar un nuevo empleado
        public void AgregarEmpleado(string nombre, string apellido, string dni, DateTime fecha, string telefono, string direccion, string usuario, string clave, int rol)
        {
            clave = Funciones.Encriptado.GenerarSHA512String(clave); // Hash de la clave
            objetoCD.Agregar(nombre, apellido, dni, fecha, telefono, direccion, usuario, clave, rol);
        }




        // Metodo para modificar un empleado seleccionado en el DG
        public void ModificarEmpleado(string nombre, string apellido, string dni, DateTime fecha, string telefono, string direccion, string clave, int rol, string legajo)
        {
            clave = Funciones.Encriptado.GenerarSHA512String(clave); // Hash de la clave
            objetoCD.Editar(nombre, apellido, dni, fecha, telefono, direccion, clave, rol, int.Parse(legajo));
        }




        // Eliminar un empleado
        public void EliminarEmpleado(string legajo)
        {
            objetoCD.Eliminar(int.Parse(legajo));
        }
    }
}
