﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Agregar Librerias
using System.Data;
using System.Data.SqlClient;
using CapaDatos;


namespace CapaNegocio
{
    // Clase PUBLICA
    public class CN_Cocina
    {
        // Instancio la clase de CAPADATOS
        private CD_Cocina objetoCD = new CD_Cocina();




        // Metodo que muestra el listado de pedidos sin entregar
        public DataTable MostrarPedidos()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.Mostrar();
            return tabla;
        }




        // Metodo para entregar un pedido seleccionado
        public void EntregarPedido(string nroPedido)
        {
            objetoCD.Entregar(Convert.ToInt32(nroPedido));
        }




        // Metodo para mostrar en el datagrid un pedido
        // La busqueda se puede hacer por numero de pedido o nombre del cliente
        public DataTable BuscarPedido(string busqueda)
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.Buscar(busqueda);
            return tabla;
        }
    }
}
