﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
// Agregar Libreria
using CapaNegocio;


namespace FormularioPrincipalLogin
{
    public partial class GridPanelCocina : Form
    {
        // Instanciamiento y declaracion de variables
        CN_Cocina objetoCN = new CN_Cocina();
        private string IDpedido = null;




        // Constructor de la clase
        public GridPanelCocina()
        {
            InitializeComponent();
            btnCocinaVerTodosLosPedidos.Visible = false;
        }




        // Al iniciarse el formulario muestra el datagrid con los pedidos sin entregar
        private void GridPanelCocina_Load(object sender, EventArgs e)
        {
            MostrarPedidos();
        }




        // Metodo que muestra los pedidos
        private void MostrarPedidos()
        {
            CN_Cocina objeto = new CN_Cocina();
            dgPedidosCocina.DataSource = objeto.MostrarPedidos();
        }




        // Buscar pedido
        private void btnCocinaBuscar_Click(object sender, EventArgs e)
        {
            // Se verifica antes que no este vacio el campo:
            if (txtCocinaBuscar.Text == "")
            {
                errorGridCocina.SetError(txtCocinaBuscar, "El campo es obligatorio.");
                txtCocinaBuscar.Focus();
                return;
            }
            errorGridCocina.SetError(txtCocinaBuscar, "");
            // Si no hay errores, llama al metodo de buscar pedidos
            MostrarPedidosBuscados();
            btnCocinaVerTodosLosPedidos.Enabled = true;
        }
        // Metodo que muestra los pedidos buscados
        // Completa nuevamente el datagrid con los datos que trajo de la DB
        private void MostrarPedidosBuscados()
        {
            CN_Caja objeto = new CN_Caja();
            dgPedidosCocina.DataSource = objeto.BuscarPedido(txtCocinaBuscar.Text);
            txtCocinaBuscar.Enabled = false;
            btnCocinaVerTodosLosPedidos.Visible = true;
            btnCocinaBuscar.Visible = false;
        }




        // Metodo que muestra todos los pedidos
        private void btnCocinaVerTodosLosPedidos_Click(object sender, EventArgs e)
        {
            LimpiarFormulario();
            MostrarPedidos();
        }




        // Entregar pedido
        private void btnCocinaEntregarPedido_Click(object sender, EventArgs e)
        {
            if (dgPedidosCocina.SelectedRows.Count > 0)
            {
                IDpedido = dgPedidosCocina.CurrentRow.Cells["NroPedido"].Value.ToString();
                objetoCN.EntregarPedido(IDpedido);
                MessageBox.Show("Pedido entregado exitosamente.");
                LimpiarFormulario();
                MostrarPedidos();
            }
            else
            {
                MessageBox.Show("Selecciona una fila.");
            }
        }




        // Metodo que limpia el formulario
        private void LimpiarFormulario()
        {
            btnCocinaVerTodosLosPedidos.Enabled = false;
            txtCocinaBuscar.Text = "";
            txtCocinaBuscar.Enabled = true;
            btnCocinaVerTodosLosPedidos.Visible = false;
            btnCocinaBuscar.Visible = true;
            errorGridCocina.Clear();
        }
    }
}
