﻿namespace FormularioPrincipalLogin
{
    partial class GridPanelCocina
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgPedidosCocina = new System.Windows.Forms.DataGridView();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnCocinaVerTodosLosPedidos = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtCocinaBuscar = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnCocinaBuscar = new System.Windows.Forms.Button();
            this.btnCocinaEntregarPedido = new System.Windows.Forms.Button();
            this.errorGridCocina = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPedidosCocina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorGridCocina)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::FormularioPrincipalLogin.Properties.Resources.logo_pachin;
            this.pictureBox1.Image = global::FormularioPrincipalLogin.Properties.Resources.logo_pachin;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(171, 76);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.label5.Location = new System.Drawing.Point(384, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 18);
            this.label5.TabIndex = 26;
            this.label5.Text = "Entrega de pedidos";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(400, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 31);
            this.label1.TabIndex = 25;
            this.label1.Text = "COCINA";
            // 
            // dgPedidosCocina
            // 
            this.dgPedidosCocina.AllowUserToAddRows = false;
            this.dgPedidosCocina.AllowUserToDeleteRows = false;
            this.dgPedidosCocina.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgPedidosCocina.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(204)))), ((int)(((byte)(173)))));
            this.dgPedidosCocina.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPedidosCocina.Location = new System.Drawing.Point(12, 94);
            this.dgPedidosCocina.Name = "dgPedidosCocina";
            this.dgPedidosCocina.ReadOnly = true;
            this.dgPedidosCocina.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPedidosCocina.Size = new System.Drawing.Size(910, 235);
            this.dgPedidosCocina.TabIndex = 27;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::FormularioPrincipalLogin.Properties.Resources.entregapedidos1;
            this.pictureBox2.Location = new System.Drawing.Point(529, 36);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(49, 36);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 28;
            this.pictureBox2.TabStop = false;
            // 
            // btnCocinaVerTodosLosPedidos
            // 
            this.btnCocinaVerTodosLosPedidos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(91)))), ((int)(((byte)(108)))));
            this.btnCocinaVerTodosLosPedidos.FlatAppearance.BorderSize = 0;
            this.btnCocinaVerTodosLosPedidos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCocinaVerTodosLosPedidos.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCocinaVerTodosLosPedidos.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCocinaVerTodosLosPedidos.Location = new System.Drawing.Point(357, 382);
            this.btnCocinaVerTodosLosPedidos.Name = "btnCocinaVerTodosLosPedidos";
            this.btnCocinaVerTodosLosPedidos.Size = new System.Drawing.Size(191, 53);
            this.btnCocinaVerTodosLosPedidos.TabIndex = 46;
            this.btnCocinaVerTodosLosPedidos.Text = "Listar pedidos";
            this.btnCocinaVerTodosLosPedidos.UseVisualStyleBackColor = false;
            this.btnCocinaVerTodosLosPedidos.Click += new System.EventHandler(this.btnCocinaVerTodosLosPedidos_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel4.Location = new System.Drawing.Point(18, 399);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(251, 2);
            this.panel4.TabIndex = 42;
            // 
            // txtCocinaBuscar
            // 
            this.txtCocinaBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtCocinaBuscar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCocinaBuscar.Font = new System.Drawing.Font("Arial", 12F);
            this.txtCocinaBuscar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtCocinaBuscar.Location = new System.Drawing.Point(18, 382);
            this.txtCocinaBuscar.Name = "txtCocinaBuscar";
            this.txtCocinaBuscar.Size = new System.Drawing.Size(251, 19);
            this.txtCocinaBuscar.TabIndex = 41;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(14, 343);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(169, 22);
            this.label7.TabIndex = 40;
            this.label7.Text = "BUSCAR PEDIDO";
            // 
            // btnCocinaBuscar
            // 
            this.btnCocinaBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.btnCocinaBuscar.FlatAppearance.BorderSize = 0;
            this.btnCocinaBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCocinaBuscar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCocinaBuscar.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCocinaBuscar.Location = new System.Drawing.Point(18, 417);
            this.btnCocinaBuscar.Name = "btnCocinaBuscar";
            this.btnCocinaBuscar.Size = new System.Drawing.Size(94, 42);
            this.btnCocinaBuscar.TabIndex = 47;
            this.btnCocinaBuscar.Text = "Buscar";
            this.btnCocinaBuscar.UseVisualStyleBackColor = false;
            this.btnCocinaBuscar.Click += new System.EventHandler(this.btnCocinaBuscar_Click);
            // 
            // btnCocinaEntregarPedido
            // 
            this.btnCocinaEntregarPedido.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(124)))), ((int)(((byte)(83)))));
            this.btnCocinaEntregarPedido.FlatAppearance.BorderSize = 0;
            this.btnCocinaEntregarPedido.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCocinaEntregarPedido.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCocinaEntregarPedido.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCocinaEntregarPedido.Location = new System.Drawing.Point(637, 382);
            this.btnCocinaEntregarPedido.Name = "btnCocinaEntregarPedido";
            this.btnCocinaEntregarPedido.Size = new System.Drawing.Size(285, 53);
            this.btnCocinaEntregarPedido.TabIndex = 48;
            this.btnCocinaEntregarPedido.Text = "Entregar Pedido";
            this.btnCocinaEntregarPedido.UseVisualStyleBackColor = false;
            this.btnCocinaEntregarPedido.Click += new System.EventHandler(this.btnCocinaEntregarPedido_Click);
            // 
            // errorGridCocina
            // 
            this.errorGridCocina.ContainerControl = this;
            // 
            // GridPanelCocina
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::FormularioPrincipalLogin.Properties.Resources.fondo_programa;
            this.ClientSize = new System.Drawing.Size(944, 501);
            this.Controls.Add(this.btnCocinaEntregarPedido);
            this.Controls.Add(this.btnCocinaBuscar);
            this.Controls.Add(this.btnCocinaVerTodosLosPedidos);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.txtCocinaBuscar);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.dgPedidosCocina);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "GridPanelCocina";
            this.Text = "GridPanelCocina";
            this.Load += new System.EventHandler(this.GridPanelCocina_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPedidosCocina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorGridCocina)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgPedidosCocina;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnCocinaVerTodosLosPedidos;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtCocinaBuscar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnCocinaBuscar;
        private System.Windows.Forms.Button btnCocinaEntregarPedido;
        private System.Windows.Forms.ErrorProvider errorGridCocina;
    }
}