﻿namespace FormularioPrincipalLogin
{
    partial class GridPanelCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.dgPedidosCaja = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtCajaCliente = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtCajaCantPanchos = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtCajaCantHamburguesas = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCajaAgregar = new System.Windows.Forms.Button();
            this.btnCajaCalcular = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtCajaBuscar = new System.Windows.Forms.TextBox();
            this.btnCajaEditar = new System.Windows.Forms.Button();
            this.btnCajaBuscar = new System.Windows.Forms.Button();
            this.btnCajaEliminar = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblCajaTotalPedido = new System.Windows.Forms.Label();
            this.errorGridCaja = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnCajaVerTodosLosPedidos = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnCancelEdicion = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgPedidosCaja)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorGridCaja)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial Black", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(422, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "CAJA";
            // 
            // dgPedidosCaja
            // 
            this.dgPedidosCaja.AllowUserToAddRows = false;
            this.dgPedidosCaja.AllowUserToDeleteRows = false;
            this.dgPedidosCaja.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgPedidosCaja.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(204)))), ((int)(((byte)(173)))));
            this.dgPedidosCaja.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPedidosCaja.GridColor = System.Drawing.Color.DarkOrange;
            this.dgPedidosCaja.Location = new System.Drawing.Point(12, 94);
            this.dgPedidosCaja.Name = "dgPedidosCaja";
            this.dgPedidosCaja.ReadOnly = true;
            this.dgPedidosCaja.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPedidosCaja.Size = new System.Drawing.Size(910, 200);
            this.dgPedidosCaja.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(87, 344);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 22);
            this.label3.TabIndex = 16;
            this.label3.Text = "Cliente:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel1.Location = new System.Drawing.Point(169, 364);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(165, 2);
            this.panel1.TabIndex = 15;
            // 
            // txtCajaCliente
            // 
            this.txtCajaCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtCajaCliente.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCajaCliente.Font = new System.Drawing.Font("Arial", 12F);
            this.txtCajaCliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtCajaCliente.Location = new System.Drawing.Point(169, 347);
            this.txtCajaCliente.Name = "txtCajaCliente";
            this.txtCajaCliente.Size = new System.Drawing.Size(165, 19);
            this.txtCajaCliente.TabIndex = 14;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::FormularioPrincipalLogin.Properties.Resources.logo_pachin;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(171, 76);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.label2.Location = new System.Drawing.Point(71, 369);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 22);
            this.label2.TabIndex = 20;
            this.label2.Text = "Panchos:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel2.Location = new System.Drawing.Point(170, 394);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(165, 2);
            this.panel2.TabIndex = 19;
            // 
            // txtCajaCantPanchos
            // 
            this.txtCajaCantPanchos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtCajaCantPanchos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCajaCantPanchos.Font = new System.Drawing.Font("Arial", 12F);
            this.txtCajaCantPanchos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtCajaCantPanchos.Location = new System.Drawing.Point(170, 374);
            this.txtCajaCantPanchos.Name = "txtCajaCantPanchos";
            this.txtCajaCantPanchos.Size = new System.Drawing.Size(165, 19);
            this.txtCajaCantPanchos.TabIndex = 18;
            this.txtCajaCantPanchos.TextChanged += new System.EventHandler(this.txtCajaCantPanchos_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.label4.Location = new System.Drawing.Point(12, 394);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(158, 22);
            this.label4.TabIndex = 23;
            this.label4.Text = "Hamburguesas:";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel3.Location = new System.Drawing.Point(170, 422);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(165, 2);
            this.panel3.TabIndex = 22;
            // 
            // txtCajaCantHamburguesas
            // 
            this.txtCajaCantHamburguesas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtCajaCantHamburguesas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCajaCantHamburguesas.Font = new System.Drawing.Font("Arial", 12F);
            this.txtCajaCantHamburguesas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtCajaCantHamburguesas.Location = new System.Drawing.Point(169, 402);
            this.txtCajaCantHamburguesas.Name = "txtCajaCantHamburguesas";
            this.txtCajaCantHamburguesas.Size = new System.Drawing.Size(167, 19);
            this.txtCajaCantHamburguesas.TabIndex = 21;
            this.txtCajaCantHamburguesas.TextChanged += new System.EventHandler(this.txtCajaCantHamburguesas_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.label5.Location = new System.Drawing.Point(392, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(157, 19);
            this.label5.TabIndex = 24;
            this.label5.Text = "Gestión de pedidos";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(8, 307);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 22);
            this.label6.TabIndex = 26;
            this.label6.Text = "NUEVO PEDIDO";
            // 
            // btnCajaAgregar
            // 
            this.btnCajaAgregar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(124)))), ((int)(((byte)(83)))));
            this.btnCajaAgregar.FlatAppearance.BorderSize = 0;
            this.btnCajaAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCajaAgregar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCajaAgregar.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCajaAgregar.Location = new System.Drawing.Point(256, 434);
            this.btnCajaAgregar.Name = "btnCajaAgregar";
            this.btnCajaAgregar.Size = new System.Drawing.Size(80, 34);
            this.btnCajaAgregar.TabIndex = 27;
            this.btnCajaAgregar.Text = "Agregar";
            this.btnCajaAgregar.UseVisualStyleBackColor = false;
            this.btnCajaAgregar.Click += new System.EventHandler(this.btnCajaAgregar_Click);
            // 
            // btnCajaCalcular
            // 
            this.btnCajaCalcular.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(91)))), ((int)(((byte)(108)))));
            this.btnCajaCalcular.FlatAppearance.BorderSize = 0;
            this.btnCajaCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCajaCalcular.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCajaCalcular.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCajaCalcular.Location = new System.Drawing.Point(164, 434);
            this.btnCajaCalcular.Name = "btnCajaCalcular";
            this.btnCajaCalcular.Size = new System.Drawing.Size(80, 34);
            this.btnCajaCalcular.TabIndex = 28;
            this.btnCajaCalcular.Text = "Calcular";
            this.btnCajaCalcular.UseVisualStyleBackColor = false;
            this.btnCajaCalcular.Click += new System.EventHandler(this.btnCajaCalcular_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(667, 307);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(169, 22);
            this.label7.TabIndex = 29;
            this.label7.Text = "BUSCAR PEDIDO";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel4.Location = new System.Drawing.Point(671, 361);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(251, 2);
            this.panel4.TabIndex = 31;
            // 
            // txtCajaBuscar
            // 
            this.txtCajaBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtCajaBuscar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCajaBuscar.Font = new System.Drawing.Font("Arial", 12F);
            this.txtCajaBuscar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtCajaBuscar.Location = new System.Drawing.Point(671, 341);
            this.txtCajaBuscar.Name = "txtCajaBuscar";
            this.txtCajaBuscar.Size = new System.Drawing.Size(251, 19);
            this.txtCajaBuscar.TabIndex = 30;
            // 
            // btnCajaEditar
            // 
            this.btnCajaEditar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(124)))), ((int)(((byte)(83)))));
            this.btnCajaEditar.FlatAppearance.BorderSize = 0;
            this.btnCajaEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCajaEditar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCajaEditar.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCajaEditar.Location = new System.Drawing.Point(756, 369);
            this.btnCajaEditar.Name = "btnCajaEditar";
            this.btnCajaEditar.Size = new System.Drawing.Size(80, 34);
            this.btnCajaEditar.TabIndex = 33;
            this.btnCajaEditar.Text = "Editar";
            this.btnCajaEditar.UseVisualStyleBackColor = false;
            this.btnCajaEditar.Click += new System.EventHandler(this.btnCajaEditar_Click);
            // 
            // btnCajaBuscar
            // 
            this.btnCajaBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.btnCajaBuscar.FlatAppearance.BorderSize = 0;
            this.btnCajaBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCajaBuscar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCajaBuscar.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCajaBuscar.Location = new System.Drawing.Point(671, 369);
            this.btnCajaBuscar.Name = "btnCajaBuscar";
            this.btnCajaBuscar.Size = new System.Drawing.Size(80, 34);
            this.btnCajaBuscar.TabIndex = 34;
            this.btnCajaBuscar.Text = "Buscar";
            this.btnCajaBuscar.UseVisualStyleBackColor = false;
            this.btnCajaBuscar.Click += new System.EventHandler(this.btnCajaBuscar_Click);
            // 
            // btnCajaEliminar
            // 
            this.btnCajaEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(86)))), ((int)(((byte)(93)))));
            this.btnCajaEliminar.FlatAppearance.BorderSize = 0;
            this.btnCajaEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCajaEliminar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCajaEliminar.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCajaEliminar.Location = new System.Drawing.Point(842, 369);
            this.btnCajaEliminar.Name = "btnCajaEliminar";
            this.btnCajaEliminar.Size = new System.Drawing.Size(80, 34);
            this.btnCajaEliminar.TabIndex = 35;
            this.btnCajaEliminar.Text = "Cancelar";
            this.btnCajaEliminar.UseVisualStyleBackColor = false;
            this.btnCajaEliminar.Click += new System.EventHandler(this.btnCajaEliminar_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(379, 307);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 22);
            this.label8.TabIndex = 36;
            this.label8.Text = "DETALLE";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.label9.Location = new System.Drawing.Point(380, 347);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(173, 22);
            this.label9.TabIndex = 37;
            this.label9.Text = "Total a facturar: $";
            // 
            // lblCajaTotalPedido
            // 
            this.lblCajaTotalPedido.AutoSize = true;
            this.lblCajaTotalPedido.BackColor = System.Drawing.Color.Transparent;
            this.lblCajaTotalPedido.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCajaTotalPedido.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.lblCajaTotalPedido.Location = new System.Drawing.Point(559, 347);
            this.lblCajaTotalPedido.Name = "lblCajaTotalPedido";
            this.lblCajaTotalPedido.Size = new System.Drawing.Size(21, 22);
            this.lblCajaTotalPedido.TabIndex = 38;
            this.lblCajaTotalPedido.Text = "0";
            // 
            // errorGridCaja
            // 
            this.errorGridCaja.ContainerControl = this;
            // 
            // btnCajaVerTodosLosPedidos
            // 
            this.btnCajaVerTodosLosPedidos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(91)))), ((int)(((byte)(108)))));
            this.btnCajaVerTodosLosPedidos.FlatAppearance.BorderSize = 0;
            this.btnCajaVerTodosLosPedidos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCajaVerTodosLosPedidos.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCajaVerTodosLosPedidos.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCajaVerTodosLosPedidos.Location = new System.Drawing.Point(671, 369);
            this.btnCajaVerTodosLosPedidos.Name = "btnCajaVerTodosLosPedidos";
            this.btnCajaVerTodosLosPedidos.Size = new System.Drawing.Size(251, 34);
            this.btnCajaVerTodosLosPedidos.TabIndex = 39;
            this.btnCajaVerTodosLosPedidos.Text = "Ver todos los pedidos";
            this.btnCajaVerTodosLosPedidos.UseVisualStyleBackColor = false;
            this.btnCajaVerTodosLosPedidos.Click += new System.EventHandler(this.btnCajaVerTodosLosPedidos_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::FormularioPrincipalLogin.Properties.Resources.comida;
            this.pictureBox2.Location = new System.Drawing.Point(546, 39);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(57, 49);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 40;
            this.pictureBox2.TabStop = false;
            // 
            // btnCancelEdicion
            // 
            this.btnCancelEdicion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(124)))), ((int)(((byte)(83)))));
            this.btnCancelEdicion.FlatAppearance.BorderSize = 0;
            this.btnCancelEdicion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelEdicion.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelEdicion.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCancelEdicion.Location = new System.Drawing.Point(671, 369);
            this.btnCancelEdicion.Name = "btnCancelEdicion";
            this.btnCancelEdicion.Size = new System.Drawing.Size(251, 34);
            this.btnCancelEdicion.TabIndex = 41;
            this.btnCancelEdicion.TabStop = false;
            this.btnCancelEdicion.Text = "Cancelar edición";
            this.btnCancelEdicion.UseVisualStyleBackColor = false;
            this.btnCancelEdicion.Visible = false;
            this.btnCancelEdicion.Click += new System.EventHandler(this.btnCancelEdicion_Click);
            // 
            // GridPanelCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::FormularioPrincipalLogin.Properties.Resources.fondo_programa;
            this.ClientSize = new System.Drawing.Size(944, 501);
            this.Controls.Add(this.btnCancelEdicion);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnCajaVerTodosLosPedidos);
            this.Controls.Add(this.lblCajaTotalPedido);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnCajaEliminar);
            this.Controls.Add(this.btnCajaBuscar);
            this.Controls.Add(this.btnCajaEditar);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.txtCajaBuscar);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnCajaCalcular);
            this.Controls.Add(this.btnCajaAgregar);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.txtCajaCantHamburguesas);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.txtCajaCantPanchos);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtCajaCliente);
            this.Controls.Add(this.dgPedidosCaja);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "GridPanelCaja";
            this.Text = "GridPanelCaja";
            this.Load += new System.EventHandler(this.GridPanelCaja_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgPedidosCaja)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorGridCaja)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgPedidosCaja;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtCajaCliente;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtCajaCantPanchos;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtCajaCantHamburguesas;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCajaAgregar;
        private System.Windows.Forms.Button btnCajaCalcular;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtCajaBuscar;
        private System.Windows.Forms.Button btnCajaEditar;
        private System.Windows.Forms.Button btnCajaBuscar;
        private System.Windows.Forms.Button btnCajaEliminar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblCajaTotalPedido;
        private System.Windows.Forms.ErrorProvider errorGridCaja;
        private System.Windows.Forms.Button btnCajaVerTodosLosPedidos;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnCancelEdicion;
    }
}