﻿namespace FormularioPrincipalLogin
{
    partial class GridPanelEmpleado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.dgEmpleados = new System.Windows.Forms.DataGridView();
            this.dgVentas = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtEmpleadoNombre = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtEmpleadoApellido = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtEmpleadoTelefono = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtEmpleadoDNI = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtEmpleadoRol = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtEmpleadoDireccion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnEmpleadoEditar = new System.Windows.Forms.Button();
            this.btnEmpleadoAgregar = new System.Windows.Forms.Button();
            this.btnEmpleadoEliminar = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.errorEmpleado = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnCancelEdicion = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtEmpleadoFecha = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtEmpleadoClave = new MisControles.CajaTxtAlphaNum();
            this.txtEmpleadoUsuario = new MisControles.CajaTxtAlphaNum();
            this.panel9 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgEmpleados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgVentas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorEmpleado)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::FormularioPrincipalLogin.Properties.Resources.logo_pachin;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(171, 76);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.label5.Location = new System.Drawing.Point(352, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(230, 19);
            this.label5.TabIndex = 28;
            this.label5.Text = "Gestion de usuarios y ventas";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial Black", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(359, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(223, 31);
            this.label1.TabIndex = 27;
            this.label1.Text = "ADMINISTRADOR";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::FormularioPrincipalLogin.Properties.Resources.gestionadmin;
            this.pictureBox2.Location = new System.Drawing.Point(588, 35);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(47, 38);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 29;
            this.pictureBox2.TabStop = false;
            // 
            // dgEmpleados
            // 
            this.dgEmpleados.AllowUserToAddRows = false;
            this.dgEmpleados.AllowUserToDeleteRows = false;
            this.dgEmpleados.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(204)))), ((int)(((byte)(173)))));
            this.dgEmpleados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEmpleados.Location = new System.Drawing.Point(12, 94);
            this.dgEmpleados.Name = "dgEmpleados";
            this.dgEmpleados.ReadOnly = true;
            this.dgEmpleados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEmpleados.Size = new System.Drawing.Size(570, 163);
            this.dgEmpleados.TabIndex = 30;
            // 
            // dgVentas
            // 
            this.dgVentas.AllowUserToAddRows = false;
            this.dgVentas.AllowUserToDeleteRows = false;
            this.dgVentas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(204)))), ((int)(((byte)(173)))));
            this.dgVentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVentas.Location = new System.Drawing.Point(12, 289);
            this.dgVentas.Name = "dgVentas";
            this.dgVentas.ReadOnly = true;
            this.dgVentas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgVentas.Size = new System.Drawing.Size(570, 193);
            this.dgVentas.TabIndex = 31;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(606, 125);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 22);
            this.label6.TabIndex = 32;
            this.label6.Text = "EMPLEADOS";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.label3.Location = new System.Drawing.Point(674, 371);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 22);
            this.label3.TabIndex = 35;
            this.label3.Text = "Rol:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel1.Location = new System.Drawing.Point(720, 178);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(165, 2);
            this.panel1.TabIndex = 34;
            // 
            // txtEmpleadoNombre
            // 
            this.txtEmpleadoNombre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtEmpleadoNombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmpleadoNombre.Font = new System.Drawing.Font("Arial", 12F);
            this.txtEmpleadoNombre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtEmpleadoNombre.Location = new System.Drawing.Point(720, 160);
            this.txtEmpleadoNombre.Name = "txtEmpleadoNombre";
            this.txtEmpleadoNombre.Size = new System.Drawing.Size(165, 19);
            this.txtEmpleadoNombre.TabIndex = 33;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel2.Location = new System.Drawing.Point(720, 205);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(165, 2);
            this.panel2.TabIndex = 37;
            // 
            // txtEmpleadoApellido
            // 
            this.txtEmpleadoApellido.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtEmpleadoApellido.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmpleadoApellido.Font = new System.Drawing.Font("Arial", 12F);
            this.txtEmpleadoApellido.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtEmpleadoApellido.Location = new System.Drawing.Point(720, 186);
            this.txtEmpleadoApellido.Name = "txtEmpleadoApellido";
            this.txtEmpleadoApellido.Size = new System.Drawing.Size(165, 19);
            this.txtEmpleadoApellido.TabIndex = 36;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel3.Location = new System.Drawing.Point(720, 284);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(165, 2);
            this.panel3.TabIndex = 41;
            // 
            // txtEmpleadoTelefono
            // 
            this.txtEmpleadoTelefono.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtEmpleadoTelefono.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmpleadoTelefono.Font = new System.Drawing.Font("Arial", 12F);
            this.txtEmpleadoTelefono.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtEmpleadoTelefono.Location = new System.Drawing.Point(720, 267);
            this.txtEmpleadoTelefono.Name = "txtEmpleadoTelefono";
            this.txtEmpleadoTelefono.Size = new System.Drawing.Size(165, 19);
            this.txtEmpleadoTelefono.TabIndex = 40;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel4.Location = new System.Drawing.Point(720, 232);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(165, 2);
            this.panel4.TabIndex = 39;
            // 
            // txtEmpleadoDNI
            // 
            this.txtEmpleadoDNI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtEmpleadoDNI.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmpleadoDNI.Font = new System.Drawing.Font("Arial", 12F);
            this.txtEmpleadoDNI.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtEmpleadoDNI.Location = new System.Drawing.Point(720, 213);
            this.txtEmpleadoDNI.Name = "txtEmpleadoDNI";
            this.txtEmpleadoDNI.Size = new System.Drawing.Size(166, 19);
            this.txtEmpleadoDNI.TabIndex = 38;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel5.Location = new System.Drawing.Point(720, 391);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(165, 2);
            this.panel5.TabIndex = 49;
            // 
            // txtEmpleadoRol
            // 
            this.txtEmpleadoRol.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtEmpleadoRol.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmpleadoRol.Font = new System.Drawing.Font("Arial", 12F);
            this.txtEmpleadoRol.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtEmpleadoRol.Location = new System.Drawing.Point(720, 372);
            this.txtEmpleadoRol.Name = "txtEmpleadoRol";
            this.txtEmpleadoRol.Size = new System.Drawing.Size(165, 19);
            this.txtEmpleadoRol.TabIndex = 48;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel6.Location = new System.Drawing.Point(720, 364);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(165, 2);
            this.panel6.TabIndex = 47;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel7.Location = new System.Drawing.Point(720, 337);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(165, 2);
            this.panel7.TabIndex = 45;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel8.Location = new System.Drawing.Point(721, 309);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(165, 2);
            this.panel8.TabIndex = 43;
            // 
            // txtEmpleadoDireccion
            // 
            this.txtEmpleadoDireccion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtEmpleadoDireccion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmpleadoDireccion.Font = new System.Drawing.Font("Arial", 12F);
            this.txtEmpleadoDireccion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtEmpleadoDireccion.Location = new System.Drawing.Point(721, 292);
            this.txtEmpleadoDireccion.Name = "txtEmpleadoDireccion";
            this.txtEmpleadoDireccion.Size = new System.Drawing.Size(165, 19);
            this.txtEmpleadoDireccion.TabIndex = 42;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.label2.Location = new System.Drawing.Point(631, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 22);
            this.label2.TabIndex = 50;
            this.label2.Text = "Nombre:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.label4.Location = new System.Drawing.Point(652, 344);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 22);
            this.label4.TabIndex = 51;
            this.label4.Text = "Clave:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.label7.Location = new System.Drawing.Point(632, 319);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 22);
            this.label7.TabIndex = 52;
            this.label7.Text = "Usuario:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.label8.Location = new System.Drawing.Point(615, 291);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 22);
            this.label8.TabIndex = 53;
            this.label8.Text = "Direccion:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.label9.Location = new System.Drawing.Point(623, 264);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 22);
            this.label9.TabIndex = 54;
            this.label9.Text = "Telefono:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.label10.Location = new System.Drawing.Point(671, 213);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 22);
            this.label10.TabIndex = 55;
            this.label10.Text = "DNI:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.label11.Location = new System.Drawing.Point(629, 186);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 22);
            this.label11.TabIndex = 56;
            this.label11.Text = "Apellido:";
            // 
            // btnEmpleadoEditar
            // 
            this.btnEmpleadoEditar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(124)))), ((int)(((byte)(83)))));
            this.btnEmpleadoEditar.FlatAppearance.BorderSize = 0;
            this.btnEmpleadoEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEmpleadoEditar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmpleadoEditar.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnEmpleadoEditar.Location = new System.Drawing.Point(720, 403);
            this.btnEmpleadoEditar.Name = "btnEmpleadoEditar";
            this.btnEmpleadoEditar.Size = new System.Drawing.Size(80, 34);
            this.btnEmpleadoEditar.TabIndex = 57;
            this.btnEmpleadoEditar.Text = "Editar";
            this.btnEmpleadoEditar.UseVisualStyleBackColor = false;
            this.btnEmpleadoEditar.Click += new System.EventHandler(this.btnEmpleadoEditar_Click);
            // 
            // btnEmpleadoAgregar
            // 
            this.btnEmpleadoAgregar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(91)))), ((int)(((byte)(108)))));
            this.btnEmpleadoAgregar.FlatAppearance.BorderSize = 0;
            this.btnEmpleadoAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEmpleadoAgregar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmpleadoAgregar.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnEmpleadoAgregar.Location = new System.Drawing.Point(805, 403);
            this.btnEmpleadoAgregar.Name = "btnEmpleadoAgregar";
            this.btnEmpleadoAgregar.Size = new System.Drawing.Size(80, 34);
            this.btnEmpleadoAgregar.TabIndex = 58;
            this.btnEmpleadoAgregar.Text = "Guardar";
            this.btnEmpleadoAgregar.UseVisualStyleBackColor = false;
            this.btnEmpleadoAgregar.Click += new System.EventHandler(this.btnEmpleadoAgregar_Click);
            // 
            // btnEmpleadoEliminar
            // 
            this.btnEmpleadoEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(86)))), ((int)(((byte)(93)))));
            this.btnEmpleadoEliminar.FlatAppearance.BorderSize = 0;
            this.btnEmpleadoEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEmpleadoEliminar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmpleadoEliminar.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnEmpleadoEliminar.Location = new System.Drawing.Point(634, 403);
            this.btnEmpleadoEliminar.Name = "btnEmpleadoEliminar";
            this.btnEmpleadoEliminar.Size = new System.Drawing.Size(80, 34);
            this.btnEmpleadoEliminar.TabIndex = 59;
            this.btnEmpleadoEliminar.Text = "Eliminar";
            this.btnEmpleadoEliminar.UseVisualStyleBackColor = false;
            this.btnEmpleadoEliminar.Click += new System.EventHandler(this.btnEmpleadoEliminar_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.label12.Location = new System.Drawing.Point(12, 264);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(217, 22);
            this.label12.TabIndex = 60;
            this.label12.Text = "REPORTE DE VENTAS";
            // 
            // errorEmpleado
            // 
            this.errorEmpleado.ContainerControl = this;
            // 
            // btnCancelEdicion
            // 
            this.btnCancelEdicion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(124)))), ((int)(((byte)(83)))));
            this.btnCancelEdicion.Enabled = false;
            this.btnCancelEdicion.FlatAppearance.BorderSize = 0;
            this.btnCancelEdicion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelEdicion.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelEdicion.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCancelEdicion.Location = new System.Drawing.Point(635, 403);
            this.btnCancelEdicion.Name = "btnCancelEdicion";
            this.btnCancelEdicion.Size = new System.Drawing.Size(166, 34);
            this.btnCancelEdicion.TabIndex = 61;
            this.btnCancelEdicion.Text = "Cancelar edición";
            this.btnCancelEdicion.UseVisualStyleBackColor = false;
            this.btnCancelEdicion.Visible = false;
            this.btnCancelEdicion.Click += new System.EventHandler(this.btn_CancelEdicion_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.label13.Location = new System.Drawing.Point(745, 452);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(140, 15);
            this.label13.TabIndex = 62;
            this.label13.Text = "ROL [2 Caja] [3 Cocina] ";
            // 
            // txtEmpleadoFecha
            // 
            this.txtEmpleadoFecha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtEmpleadoFecha.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmpleadoFecha.Font = new System.Drawing.Font("Arial", 12F);
            this.txtEmpleadoFecha.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtEmpleadoFecha.Location = new System.Drawing.Point(720, 240);
            this.txtEmpleadoFecha.Name = "txtEmpleadoFecha";
            this.txtEmpleadoFecha.Size = new System.Drawing.Size(165, 19);
            this.txtEmpleadoFecha.TabIndex = 63;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.label14.Location = new System.Drawing.Point(606, 239);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(114, 22);
            this.label14.TabIndex = 64;
            this.label14.Text = "Fecha Nac:";
            // 
            // txtEmpleadoClave
            // 
            this.txtEmpleadoClave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtEmpleadoClave.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmpleadoClave.Font = new System.Drawing.Font("Arial", 12F);
            this.txtEmpleadoClave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtEmpleadoClave.Location = new System.Drawing.Point(721, 345);
            this.txtEmpleadoClave.Name = "txtEmpleadoClave";
            this.txtEmpleadoClave.Size = new System.Drawing.Size(165, 19);
            this.txtEmpleadoClave.TabIndex = 46;
            this.txtEmpleadoClave.TextChanged += new System.EventHandler(this.txtEmpleadoClave_TextChanged);
            // 
            // txtEmpleadoUsuario
            // 
            this.txtEmpleadoUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtEmpleadoUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmpleadoUsuario.Font = new System.Drawing.Font("Arial", 12F);
            this.txtEmpleadoUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtEmpleadoUsuario.Location = new System.Drawing.Point(721, 320);
            this.txtEmpleadoUsuario.Name = "txtEmpleadoUsuario";
            this.txtEmpleadoUsuario.Size = new System.Drawing.Size(165, 19);
            this.txtEmpleadoUsuario.TabIndex = 44;
            this.txtEmpleadoUsuario.TextChanged += new System.EventHandler(this.txtEmpleadoUsuario_TextChanged);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel9.Location = new System.Drawing.Point(720, 259);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(165, 2);
            this.panel9.TabIndex = 40;
            // 
            // GridPanelEmpleado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::FormularioPrincipalLogin.Properties.Resources.fondo_programa;
            this.ClientSize = new System.Drawing.Size(944, 501);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtEmpleadoFecha);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btnCancelEdicion);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnEmpleadoEliminar);
            this.Controls.Add(this.btnEmpleadoAgregar);
            this.Controls.Add(this.btnEmpleadoEditar);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.txtEmpleadoRol);
            this.Controls.Add(this.txtEmpleadoTelefono);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.txtEmpleadoClave);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.txtEmpleadoUsuario);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.txtEmpleadoDNI);
            this.Controls.Add(this.txtEmpleadoDireccion);
            this.Controls.Add(this.txtEmpleadoApellido);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtEmpleadoNombre);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dgVentas);
            this.Controls.Add(this.dgEmpleados);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "GridPanelEmpleado";
            this.Text = "GridPanelEmpleado";
            this.Load += new System.EventHandler(this.GridPanelEmpleado_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgEmpleados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgVentas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorEmpleado)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.DataGridView dgEmpleados;
        private System.Windows.Forms.DataGridView dgVentas;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtEmpleadoNombre;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtEmpleadoApellido;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtEmpleadoTelefono;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtEmpleadoDNI;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtEmpleadoRol;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private MisControles.CajaTxtAlphaNum txtEmpleadoClave;
        private MisControles.CajaTxtAlphaNum txtEmpleadoUsuario;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox txtEmpleadoDireccion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnEmpleadoEditar;
        private System.Windows.Forms.Button btnEmpleadoAgregar;
        private System.Windows.Forms.Button btnEmpleadoEliminar;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnCancelEdicion;
        private System.Windows.Forms.ErrorProvider errorEmpleado;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtEmpleadoFecha;
        private System.Windows.Forms.Panel panel9;
    }
}