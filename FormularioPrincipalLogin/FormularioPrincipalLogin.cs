﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using Funciones;


namespace FormularioPrincipalLogin
{
    public partial class FormularioPrincipalLogin : Form
    {
        // Se permite 3 intentos y se cierra el programa
        private int cantidadIntentos = 0;




        public FormularioPrincipalLogin()
        {
            InitializeComponent();
        }




        // Boton salid
        private void picSalir_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Fin del programa: PACHIN 2.0. \n\nBy AMSAM Software Factory:\n\tMelisa Amaike\n\tFernando Melo\n\tJoaquin Santos");
            Application.Exit();
        }




        // Boton ingresar
        private void btnLoginIngresar_Click(object sender, EventArgs e)
        {
            // Validacion de campo vacio para el usuario
            if (txtboxLoginUsuario.Text == "")
            {
                errorIngresoUsuario.SetError(txtboxLoginUsuario, "Debes ingresar el usuario.");
                txtboxLoginUsuario.Focus();
                return;
            }
            errorIngresoUsuario.SetError(txtboxLoginUsuario, "");
            // Validacion de campo vacio para la clave
            if (txtboxLoginClave.Text == "")
            {
                errorIngresoUsuario.SetError(txtboxLoginClave, "Debes ingresar la clave.");
                txtboxLoginClave.Focus();
                return;
            }
            errorIngresoUsuario.SetError(txtboxLoginClave, "");
            // NUEVO PARA CONTROLAR LOGIN
            int Ingreso = ValidarIngreso(txtboxLoginUsuario.Text, (Funciones.Encriptado.GenerarSHA512String(txtboxLoginClave.Text)));
            if (Ingreso != -1)
            {
                Hide();
                FormularioIngresoUsuario abrir = new FormularioIngresoUsuario(Ingreso);
                abrir.ShowDialog(); // Mientras el form correspondiente siga abierto, no se ve el login
                txtboxLoginClave.Text = "";
                txtboxLoginUsuario.Text = "";
                Show();
            }
            else
            {
                // No se logro logear
                MessageBox.Show("Intente de nuevo, los datos no son correctos");
                cantidadIntentos++;
                // Cierra el programa luego de 3 intentos
                if (cantidadIntentos == 3)
                {
                    MessageBox.Show("Cantidad maxima de intentos superada.");
                    Application.Exit();
                }
                // Limpia los campos
                txtboxLoginUsuario.Text = "";
                txtboxLoginClave.Text = "";
            }
        }




        //Validacion de login
        //Devuelve el rol en caso de logeo exitoso y -1 caso contrario
        public int ValidarIngreso(string usuario, string clave) 
        {
            CN_Empleado metodos = new CN_Empleado();
            DataTable tabla = metodos.MostrarEmpleados();
            foreach (DataRow fila in tabla.Rows)
            {
                if (fila["Usuario"].ToString() == usuario && fila["Clave"].ToString() == clave)
                {
                    return int.Parse(fila["Rol"].ToString());
                }
            }
            return -1;
        }




        // Validacion por capa de controles que el usuario ingresado solo tenga caracteres alfanumericos
        private void txtboxLoginUsuario_TextChanged(object sender, EventArgs e)
        {
            txtboxLoginUsuario.ValidarTexto();
        }




        // Validacion por capa de controles que la clave ingresada solo tenga caracteres alfanumericos
        private void txtboxLoginClave_TextChanged(object sender, EventArgs e)
        {
            txtboxLoginClave.ValidarTexto();
        }
    }
}
