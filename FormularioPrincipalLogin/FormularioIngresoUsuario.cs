﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace FormularioPrincipalLogin
{
    public partial class FormularioIngresoUsuario : Form
    {
        // El rol del empleado es 1 para admin, 2 caja y 3 cocina
        private int rolEmpleado = 0;




        public FormularioIngresoUsuario(int rol)
        {
            InitializeComponent();
            rolEmpleado = rol;
        }




        // Carga del formulario segun rol de usuario
        private void FormularioIngresoUsuario_Load(object sender, EventArgs e)
        {
            switch (rolEmpleado) 
            {
                case 1:
                    AbrirPanel(new GridPanelEmpleado());
                    break;
                case 2:
                    AbrirPanel(new GridPanelCaja());
                    break;
                case 3:
                    AbrirPanel(new GridPanelCocina());
                    break;
            } 
        }




        // Con este metodo hago que los formularios hijos abran en el grid panel
        private void AbrirPanel(object frmHijo)
        {
            if (panelContenedor.Controls.Count > 0)
                panelContenedor.Controls.RemoveAt(0);
            Form fh = frmHijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill; // acomoda el formulario al panel
            panelContenedor.Controls.Add(fh);
            panelContenedor.Tag = fh;
            fh.Show();
        }
    }
}
