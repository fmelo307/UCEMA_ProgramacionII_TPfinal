﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
// Agregar Libreria
using CapaNegocio;
using Excepciones;


namespace FormularioPrincipalLogin
{
    public partial class GridPanelCaja : Form
    {
        // Instanciamiento y declaracion de variables
        CN_Caja objetoCN = new CN_Caja();
        private string IDpedido = null;
        private bool Editar = false;




        // Constructor clase
        public GridPanelCaja()
        {
            InitializeComponent();
            btnCajaAgregar.Enabled = false; // Se activa al calcular o al editar
            btnCajaVerTodosLosPedidos.Visible = false;
        }




        // Al iniciarse el formulario muestra el datagrid con los pedidos sin entregar
        private void GridPanelCaja_Load(object sender, EventArgs e)
        {
            MostrarPedidos();
        }




        // Metodo que muestra los pedidos
        private void MostrarPedidos()
        {
            CN_Caja objeto = new CN_Caja();
            dgPedidosCaja.DataSource = objeto.MostrarPedidos();
        }




        // Calculo del precio del pedido
        private void btnCajaCalcular_Click(object sender, EventArgs e)
        {
            // Precio establecido y total
            float precioPancho = 80;
            float precioHamburguesa = 140;
            int cantidadPanchos;
            int cantidadHamburguesas;
            float total;
            // Validacion de ingreso del cliente
            if (txtCajaCliente.Text == "")
            {
                errorGridCaja.SetError(txtCajaCliente, "Debes ingresa el nombre del cliente.");
                txtCajaCliente.Focus();
                return;
            }
            errorGridCaja.SetError(txtCajaCliente, "");
            // Validaciones de campos y casteo
            try 
            {
                if (txtCajaCantPanchos.Text == "") // Si se deja vacio se asume 0
                {
                    cantidadPanchos = 0;
                }
                else
                {
                    if (!txtCajaCantPanchos.Text.All(char.IsDigit)) // Verifica que sean solo digitos
                    {
                        throw new ExExpectPosInt();
                    }
                    else 
                    {
                        cantidadPanchos = Convert.ToInt32(txtCajaCantPanchos.Text);
                    }
                }
                if (txtCajaCantHamburguesas.Text == "") // Si se deja vacio se asume 0
                {
                    cantidadHamburguesas = 0;
                }
                else
                {
                    if (!txtCajaCantHamburguesas.Text.All(char.IsDigit)) // Verifica que sean solo digitos
                    {
                        throw new ExExpectPosInt();
                    }
                    else
                    {
                        cantidadHamburguesas = Convert.ToInt32(txtCajaCantHamburguesas.Text);
                    }
                }
                // El total no puede ser cero, sino no es pedido
                total = cantidadPanchos * precioPancho + cantidadHamburguesas * precioHamburguesa;
                if (cantidadHamburguesas < 0 || cantidadPanchos < 0) // Las cantidades no pueden ser negativas
                {
                    throw new ExExpectPosInt();
                }
                if (total == 0)
                {
                    errorGridCaja.SetError(txtCajaCantPanchos, "El total no puede ser $ 0.");
                    txtCajaCantPanchos.Focus();
                    return;
                }
                errorGridCaja.SetError(txtCajaCantPanchos, "");
                // Muestra el total en pantalla
                lblCajaTotalPedido.Text = total.ToString();
                // Habilita el boton de agregar pedido
                btnCajaAgregar.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().FullName);
            }
        }




        // Al seleccionar cualquier fila, este metodo trae al formulario los
        // datos del pedido seleccionado. De esta manera se puede editar o cancelar
        private void btnCajaEditar_Click(object sender, EventArgs e)
        {
            if (dgPedidosCaja.SelectedRows.Count > 0)
            {
                Editar = true;
                btnCajaBuscar.Visible = false;
                btnCajaEditar.Visible = false;
                btnCajaEliminar.Visible = false;
                btnCajaVerTodosLosPedidos.Visible = false;
                txtCajaBuscar.Enabled = false;
                txtCajaCliente.Text = dgPedidosCaja.CurrentRow.Cells["NombreCliente"].Value.ToString();
                txtCajaCantPanchos.Text = dgPedidosCaja.CurrentRow.Cells["CantidadPanchos"].Value.ToString();
                txtCajaCantHamburguesas.Text = dgPedidosCaja.CurrentRow.Cells["CantidadHamburguesas"].Value.ToString();
                IDpedido = dgPedidosCaja.CurrentRow.Cells["NroPedido"].Value.ToString();
                btnCancelEdicion.Visible = true;
                btnCancelEdicion.Enabled = true;
            }
            else
            {
                MessageBox.Show("Selecciona una fila.");
            }
        }




        // Boton que llama al metodo para guardar o editar un pedido
        private void btnCajaAgregar_Click(object sender, EventArgs e)
        {
            int cantidadPanchos = 0;
            int cantidadHamburguesas = 0;
            float total = float.Parse(lblCajaTotalPedido.Text);
            // NUEVO PEDIDO
            if (Editar == false)
            {
                // Comienza con el try para agregar un pedido nuevo
                try
                {
                    // Validaciones de campos
                    if (txtCajaCantPanchos.Text == "")
                    {
                        cantidadPanchos = 0;
                    }
                    else
                    {
                        cantidadPanchos = Convert.ToInt32(txtCajaCantPanchos.Text);
                    }
                    if (txtCajaCantHamburguesas.Text == "")
                    {
                        cantidadHamburguesas = 0;
                    }
                    else
                    {
                        cantidadHamburguesas = Convert.ToInt32(txtCajaCantHamburguesas.Text);
                    }
                    objetoCN.AgregarPedido(txtCajaCliente.Text, cantidadPanchos, cantidadHamburguesas, total);
                    MessageBox.Show("Se agrego el pedido correctamente.");
                    LimpiarFormulario();
                    MostrarPedidos();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex);
                }
            }
            // EDITAR UN PEDIDO
            if (Editar == true)
            {
                try
                {
                    // Validaciones de campos
                    if (txtCajaCantPanchos.Text == "")
                    {
                        cantidadPanchos = 0;
                    }
                    else
                    {
                        cantidadPanchos = Convert.ToInt32(txtCajaCantPanchos.Text);
                    }
                    if (txtCajaCantHamburguesas.Text == "")
                    {
                        cantidadHamburguesas = 0;
                    }
                    else
                    {
                        cantidadHamburguesas = Convert.ToInt32(txtCajaCantHamburguesas.Text);
                    }
                    objetoCN.ModificarPedido(txtCajaCliente.Text, cantidadPanchos, cantidadHamburguesas, total, IDpedido);
                    MessageBox.Show("Pedido actualizado con exito.");
                    LimpiarFormulario();
                    MostrarPedidos();
                    Editar = false;
                    btnCancelEdicion.Enabled = false;
                    btnCancelEdicion.Visible = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message, ex.GetType().FullName);
                }
            }
        }




        // Buscar pedido
        private void btnCajaBuscar_Click(object sender, EventArgs e)
        {
            // Se verifica antes que no este vacio el campo:
            if (txtCajaBuscar.Text == "")
            {
                errorGridCaja.SetError(txtCajaBuscar, "El campo es obligatorio.");
                txtCajaBuscar.Focus();
                return;
            }
            errorGridCaja.SetError(txtCajaBuscar, "");
            // Si no hay errores, llama al metodo de buscar pedidos
            MostrarPedidosBuscados();
            btnCajaVerTodosLosPedidos.Enabled = true;
            txtCajaBuscar.Enabled = false;
        }
        // Metodo que muestra los pedidos buscados
        // Completa nuevamente el datagrid con los datos que trajo de la DB
        private void MostrarPedidosBuscados()
        {
            CN_Caja objeto = new CN_Caja();
            dgPedidosCaja.DataSource = objeto.BuscarPedido(txtCajaBuscar.Text);
            btnCajaVerTodosLosPedidos.Visible = true;
        }




        // Metodo que muestra todos los pedidos
        private void btnCajaVerTodosLosPedidos_Click(object sender, EventArgs e)
        {
            LimpiarFormulario();
            MostrarPedidos();
        }




        // Metodo que cancela un pedido
        // Lo que hace es dejar el registro pero con las cantidades en cero
        // El total facturado en 0, y el estado como entregado (para que no se vea en el DG)
        private void btnCajaEliminar_Click(object sender, EventArgs e)
        {
            if (dgPedidosCaja.SelectedRows.Count > 0)
            {
                string cliente = dgPedidosCaja.CurrentRow.Cells["NombreCliente"].Value.ToString();
                IDpedido = dgPedidosCaja.CurrentRow.Cells["NroPedido"].Value.ToString();
                objetoCN.CancelarPedido(cliente, IDpedido);
                MessageBox.Show("Pedido cancelado exitosamente.");
                LimpiarFormulario();
                MostrarPedidos();
            }
            else
            {
                MessageBox.Show("Selecciona una fila.");
            }
        }




        // Metodo que limpia el formulario
        private void LimpiarFormulario()
        {
            btnCajaAgregar.Enabled = false;
            btnCajaVerTodosLosPedidos.Enabled = false;
            txtCajaCliente.Text = "";
            txtCajaCantPanchos.Text = "";
            txtCajaCantHamburguesas.Text = "";
            lblCajaTotalPedido.Text = "0";
            txtCajaBuscar.Text = "";
            btnCancelEdicion.Visible = false;
            btnCajaBuscar.Visible = true;
            btnCajaEditar.Visible = true;
            btnCajaEliminar.Visible = true;
            btnCajaVerTodosLosPedidos.Visible = true;
            txtCajaBuscar.Enabled = true;
            btnCajaVerTodosLosPedidos.Visible = false;
            txtCajaBuscar.Enabled = true;
            errorGridCaja.Clear();
        }




        // Boton cancelar edicion
        private void btnCancelEdicion_Click(object sender, EventArgs e)
        {
            LimpiarFormulario();
            MostrarPedidos();
            Editar = false;
        }




        // Validacion por capa de controles que la cantidad de panchos sea entera positiva
        private void txtCajaCantPanchos_TextChanged(object sender, EventArgs e)
        {
            btnCajaAgregar.Enabled = false;
        }




        // Validacion por capa de controles que la cantidad de hamburguesas sea entera positiva
        private void txtCajaCantHamburguesas_TextChanged(object sender, EventArgs e)
        {
            btnCajaAgregar.Enabled = false;
        }
    }
}
