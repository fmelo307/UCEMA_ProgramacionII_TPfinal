﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
// Agregar Libreria
using CapaNegocio;
using System.Text.RegularExpressions;
using System.Globalization;
using Excepciones;


namespace FormularioPrincipalLogin
{
    public partial class GridPanelEmpleado : Form
    {
        // Instanciamiento y declaracion de variables
        CN_Empleado objetoCNE = new CN_Empleado();
        private string IDempleado = null;
        private bool Editar = false;




        // Constructor de la clase
        public GridPanelEmpleado()
        {
            InitializeComponent();
        }




        // Al iniciarse el formulario, se muestran el DG de empleados y ventas
        private void GridPanelEmpleado_Load(object sender, EventArgs e)
        {
            ListarEmpleados();
            ListarVentas();
        }




        // Metodo que muestra a los empleados
        private void ListarEmpleados()
        {
            CN_Empleado objeto = new CN_Empleado();
            dgEmpleados.DataSource = objeto.MostrarEmpleados();
        }




        // Metodo que muestra las ventas realizadas
        private void ListarVentas()
        {
            CN_Caja objeto = new CN_Caja();
            dgVentas.DataSource = objeto.ListarVentas();
        }




        // Metodo para modificar empleados
        // El legajo y el usuario NO pueden ser modificados
        private void btnEmpleadoEditar_Click(object sender, EventArgs e)
        {
            if (dgEmpleados.SelectedRows.Count > 0)
            {
                Editar = true;
                btnEmpleadoEditar.Visible = false;
                btnEmpleadoEliminar.Visible = false;
                txtEmpleadoNombre.Text = dgEmpleados.CurrentRow.Cells["Nombre"].Value.ToString();
                txtEmpleadoApellido.Text = dgEmpleados.CurrentRow.Cells["Apellido"].Value.ToString();
                txtEmpleadoDNI.Text = dgEmpleados.CurrentRow.Cells["DNI"].Value.ToString();
                txtEmpleadoFecha.Text = dgEmpleados.CurrentRow.Cells["Fecha"].Value.ToString();
                txtEmpleadoTelefono.Text = dgEmpleados.CurrentRow.Cells["Telefono"].Value.ToString();
                txtEmpleadoDireccion.Text = dgEmpleados.CurrentRow.Cells["Direccion"].Value.ToString();
                txtEmpleadoUsuario.Text = dgEmpleados.CurrentRow.Cells["Usuario"].Value.ToString();
                //txtEmpleadoClave.Text = dgEmpleados.CurrentRow.Cells["Clave"].Value.ToString(); // No tiene sentido mostrar la clave encriptada
                txtEmpleadoRol.Text = dgEmpleados.CurrentRow.Cells["Rol"].Value.ToString();
                IDempleado = dgEmpleados.CurrentRow.Cells["Legajo"].Value.ToString();
                txtEmpleadoUsuario.Enabled = false;
                btnCancelEdicion.Visible = true;
                btnCancelEdicion.Enabled = true;
            }
            else
            {
                MessageBox.Show("Selecciona una fila.");
            }
        }




        // Metodo para agregar/modificar empleados
        private void btnEmpleadoAgregar_Click(object sender, EventArgs e)
        {
            Regex re = new Regex("\\d{2}-\\d{2}-\\d{4}"); // Expresion regular para parsear fecha
            // ALTA EMPLEADO
            if (Editar == false)
            {
                // Antes de agregar el empleado, verifico que no queden campos vacios
                // Campo Nombre
                if (txtEmpleadoNombre.Text == "")
                {
                    errorEmpleado.SetError(txtEmpleadoNombre, "El campo es obligatorio.");
                    txtEmpleadoNombre.Focus();
                    return;
                }
                else 
                {
                    if(!ValidarUsuario()) return; // Verifica si no se quiere agregar otro 'admin'
                }
                errorEmpleado.SetError(txtEmpleadoNombre, "");
                // Campo Apellido
                if (txtEmpleadoApellido.Text == "")
                {
                    errorEmpleado.SetError(txtEmpleadoApellido, "El campo es obligatorio.");
                    txtEmpleadoApellido.Focus();
                    return;
                }
                errorEmpleado.SetError(txtEmpleadoApellido, "");
                // Campo DNI
                if (txtEmpleadoDNI.Text == "")
                {
                    errorEmpleado.SetError(txtEmpleadoDNI, "El campo es obligatorio.");
                    txtEmpleadoDNI.Focus();
                    return;
                }
                errorEmpleado.SetError(txtEmpleadoDNI, "");
                // Campo Fecha
                if (txtEmpleadoFecha.Text == "" || !re.IsMatch(txtEmpleadoFecha.Text))
                {
                    errorEmpleado.SetError(txtEmpleadoFecha, "El campo es obligatorio y debe cumplir con el formato 'dd-mm-yyyy'.");
                    txtEmpleadoFecha.Focus();
                    return;
                }
                else 
                {
                    if (1900 > DateTime.ParseExact(txtEmpleadoFecha.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture).Year || DateTime.Now.Year < DateTime.ParseExact(txtEmpleadoFecha.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture).Year) 
                    {
                        errorEmpleado.SetError(txtEmpleadoFecha, "El año de la fecha no puede ser previo a 1900 ni posterior al año actual");
                        txtEmpleadoFecha.Focus();
                        return;
                    }
                }
                errorEmpleado.SetError(txtEmpleadoFecha, "");
                // Campo Telefono
                if (txtEmpleadoTelefono.Text == "")
                {
                    errorEmpleado.SetError(txtEmpleadoTelefono, "El campo es obligatorio.");
                    txtEmpleadoTelefono.Focus();
                    return;
                }
                errorEmpleado.SetError(txtEmpleadoTelefono, "");
                // Campo Direccion
                if (txtEmpleadoDireccion.Text == "")
                {
                    errorEmpleado.SetError(txtEmpleadoDireccion, "El campo es obligatorio.");
                    txtEmpleadoDireccion.Focus();
                    return;
                }
                errorEmpleado.SetError(txtEmpleadoDireccion, "");
                // Campo Usuario
                if (txtEmpleadoUsuario.Text == "")
                {
                    errorEmpleado.SetError(txtEmpleadoUsuario, "El campo es obligatorio. Se sugiere inicial del nombre y apellido.");
                    txtEmpleadoUsuario.Focus();
                    return;
                }
                errorEmpleado.SetError(txtEmpleadoUsuario, "");
                if (!ValidarRolClave()) return; // Campo clave y rol
                errorEmpleado.SetError(txtEmpleadoRol, "");
                // Comienza con el try para agregar un nuevo empleado...
                try
                {
                    objetoCNE.AgregarEmpleado(txtEmpleadoNombre.Text, txtEmpleadoApellido.Text, txtEmpleadoDNI.Text, DateTime.ParseExact(txtEmpleadoFecha.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture), txtEmpleadoTelefono.Text, txtEmpleadoDireccion.Text, txtEmpleadoUsuario.Text, txtEmpleadoClave.Text, int.Parse(txtEmpleadoRol.Text));
                    MessageBox.Show("Se agrego el empleado correctamente.");
                    ListarEmpleados();
                    LimpiarFormulario();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message, ex.GetType().FullName);
                }
            }
            // EDITAR UN EMPLEADO
            if (Editar == true)
            {
                if (txtEmpleadoFecha.Text == "" || !re.IsMatch(txtEmpleadoFecha.Text))
                {
                    errorEmpleado.SetError(txtEmpleadoFecha, "Debe cumplir con el formato 'dd-mm-yyyy'.");
                    txtEmpleadoFecha.Focus();
                    return;
                }
                if (!ValidarRolClave()) return; // Campo clave y rol
                try
                {
                    objetoCNE.ModificarEmpleado(txtEmpleadoNombre.Text, txtEmpleadoApellido.Text, txtEmpleadoDNI.Text, DateTime.ParseExact(txtEmpleadoFecha.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture), txtEmpleadoTelefono.Text, txtEmpleadoDireccion.Text, txtEmpleadoClave.Text, int.Parse(txtEmpleadoRol.Text), IDempleado);
                    MessageBox.Show("Empleado actualizado exitosamente.");
                    ListarEmpleados();
                    LimpiarFormulario();
                    Editar = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message, ex.GetType().FullName);
                }
            }
        }




        // Metodo para eliminar un empleado
        // La eliminacion es fisica
        private void btnEmpleadoEliminar_Click(object sender, EventArgs e)
        {
            if (dgEmpleados.SelectedRows.Count > 0)
            {
                if (dgEmpleados.CurrentRow.Cells["Rol"].Value.ToString() == "1") // Verifica que no se quiera eliminar al admin por error
                {
                    MessageBox.Show("No se puede eliminar al admin");
                    return;
                }
                IDempleado = dgEmpleados.CurrentRow.Cells["Legajo"].Value.ToString();
                objetoCNE.EliminarEmpleado(IDempleado);
                MessageBox.Show("Empleado eliminado exitosamente.");
                ListarEmpleados();
                LimpiarFormulario();
            }
            else
            {
                MessageBox.Show("Selecciona una fila.");
            }
        }




        // Limpiar el formulario
        private void LimpiarFormulario()
        {
            txtEmpleadoNombre.Text = "";
            txtEmpleadoApellido.Text = "";
            txtEmpleadoDNI.Text = "";
            txtEmpleadoFecha.Text = "";
            txtEmpleadoTelefono.Text = "";
            txtEmpleadoDireccion.Text = "";
            txtEmpleadoUsuario.Text = "";
            txtEmpleadoClave.Text = "";
            txtEmpleadoRol.Text = "";
            txtEmpleadoUsuario.Enabled = true;
            btnCancelEdicion.Visible = false;
            btnEmpleadoEditar.Visible = true;
            btnEmpleadoEliminar.Visible = true;
        }




        // Boton cancelar edicion
        private void btn_CancelEdicion_Click(object sender, EventArgs e)
        {
            ListarEmpleados();
            LimpiarFormulario();
            Editar = false;
            errorEmpleado.Clear();
        }




        // Validacion del Rol ingresado (caja/cocina)
        private bool ValidarRolClave()
        {
            // Campo Clave
            if (txtEmpleadoClave.Text == "")
            {
                errorEmpleado.SetError(txtEmpleadoClave, "El campo es obligatorio.");
                txtEmpleadoClave.Focus();
                return false;
            }
            errorEmpleado.SetError(txtEmpleadoClave, "");
            // Campo Rol
            if (txtEmpleadoRol.Text == "")
            {
                errorEmpleado.SetError(txtEmpleadoRol, "El campo es obligatorio.");
                txtEmpleadoRol.Focus();
                return false;
            }
            else
            {
                int tmp;
                if (!int.TryParse(txtEmpleadoRol.Text, out tmp) || tmp < 1 || tmp > 3) // Verifico que el rol sea int y este entre 1 y 3
                {
                    errorEmpleado.SetError(txtEmpleadoRol, "El rol debe ser un numero entre 1 y 3.");
                    txtEmpleadoRol.Focus();
                    return false;
                }
                else 
                {
                    try
                    {
                        if (tmp == 1 && txtEmpleadoUsuario.Text != "admin") throw new ExRolNoValido();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex.Message, ex.GetType().FullName);
                        return false;
                    }
                }
            }
            return true;
        }




        // Validacion que el usuario no sea "admin"
        private bool ValidarUsuario() 
        {
            try
            {
                if (txtEmpleadoUsuario.Text == "admin") throw new ExUsuarioNoPermitido();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, ex.GetType().FullName);
                return false;
            }
            return true;
        }




        // Validacion de ingreso de caracteres alfanumericos por capa de controles
        private void txtEmpleadoUsuario_TextChanged(object sender, EventArgs e)
        {
            txtEmpleadoUsuario.ValidarTexto();
        }




        // Validacion de ingreso de caracteres alfanumericos por capa de controles
        private void txtEmpleadoClave_TextChanged(object sender, EventArgs e)
        {
            txtEmpleadoClave.ValidarTexto();
        }
    }
}
