﻿namespace FormularioPrincipalLogin
{
    partial class FormularioPrincipalLogin
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormularioPrincipalLogin));
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtboxLoginClave = new MisControles.CajaTxtAlphaNum();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtboxLoginUsuario = new MisControles.CajaTxtAlphaNum();
            this.btnLoginIngresar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.errorIngresoUsuario = new System.Windows.Forms.ErrorProvider(this.components);
            this.picSalir = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorIngresoUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSalir)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(30, 196);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 22);
            this.label3.TabIndex = 17;
            this.label3.Text = "Clave:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel2.Location = new System.Drawing.Point(98, 216);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(165, 2);
            this.panel2.TabIndex = 16;
            // 
            // txtboxLoginClave
            // 
            this.txtboxLoginClave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtboxLoginClave.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtboxLoginClave.Font = new System.Drawing.Font("Arial", 12F);
            this.txtboxLoginClave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtboxLoginClave.Location = new System.Drawing.Point(98, 196);
            this.txtboxLoginClave.Name = "txtboxLoginClave";
            this.txtboxLoginClave.PasswordChar = '*';
            this.txtboxLoginClave.Size = new System.Drawing.Size(165, 19);
            this.txtboxLoginClave.TabIndex = 15;
            this.txtboxLoginClave.TextChanged += new System.EventHandler(this.txtboxLoginClave_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(10, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 22);
            this.label1.TabIndex = 13;
            this.label1.Text = "Usuario:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(72)))), ((int)(((byte)(17)))));
            this.panel1.Location = new System.Drawing.Point(98, 178);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(165, 2);
            this.panel1.TabIndex = 12;
            // 
            // txtboxLoginUsuario
            // 
            this.txtboxLoginUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(60)))));
            this.txtboxLoginUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtboxLoginUsuario.Font = new System.Drawing.Font("Arial", 12F);
            this.txtboxLoginUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(42)))), ((int)(((byte)(8)))));
            this.txtboxLoginUsuario.Location = new System.Drawing.Point(98, 158);
            this.txtboxLoginUsuario.Name = "txtboxLoginUsuario";
            this.txtboxLoginUsuario.Size = new System.Drawing.Size(165, 19);
            this.txtboxLoginUsuario.TabIndex = 11;
            this.txtboxLoginUsuario.TextChanged += new System.EventHandler(this.txtboxLoginUsuario_TextChanged);
            // 
            // btnLoginIngresar
            // 
            this.btnLoginIngresar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(124)))), ((int)(((byte)(83)))));
            this.btnLoginIngresar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(124)))), ((int)(((byte)(83)))));
            this.btnLoginIngresar.FlatAppearance.BorderSize = 0;
            this.btnLoginIngresar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoginIngresar.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoginIngresar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(51)))), ((int)(((byte)(50)))));
            this.btnLoginIngresar.Location = new System.Drawing.Point(75, 252);
            this.btnLoginIngresar.Name = "btnLoginIngresar";
            this.btnLoginIngresar.Size = new System.Drawing.Size(142, 41);
            this.btnLoginIngresar.TabIndex = 10;
            this.btnLoginIngresar.Text = "INGRESAR";
            this.btnLoginIngresar.UseVisualStyleBackColor = false;
            this.btnLoginIngresar.Click += new System.EventHandler(this.btnLoginIngresar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::FormularioPrincipalLogin.Properties.Resources.logo_pachin;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(263, 114);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // errorIngresoUsuario
            // 
            this.errorIngresoUsuario.ContainerControl = this;
            // 
            // picSalir
            // 
            this.picSalir.BackColor = System.Drawing.Color.Transparent;
            this.picSalir.Image = global::FormularioPrincipalLogin.Properties.Resources.UI17_512;
            this.picSalir.Location = new System.Drawing.Point(12, 322);
            this.picSalir.Name = "picSalir";
            this.picSalir.Size = new System.Drawing.Size(25, 25);
            this.picSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSalir.TabIndex = 18;
            this.picSalir.TabStop = false;
            this.picSalir.Click += new System.EventHandler(this.picSalir_Click);
            // 
            // FormularioPrincipalLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::FormularioPrincipalLogin.Properties.Resources.fondo_programa;
            this.ClientSize = new System.Drawing.Size(288, 359);
            this.ControlBox = false;
            this.Controls.Add(this.picSalir);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.txtboxLoginClave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtboxLoginUsuario);
            this.Controls.Add(this.btnLoginIngresar);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormularioPrincipalLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PACHIN 2.0";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorIngresoUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSalir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private MisControles.CajaTxtAlphaNum txtboxLoginClave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private MisControles.CajaTxtAlphaNum txtboxLoginUsuario;
        private System.Windows.Forms.Button btnLoginIngresar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ErrorProvider errorIngresoUsuario;
        private System.Windows.Forms.PictureBox picSalir;
    }
}

