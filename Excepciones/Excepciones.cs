﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Runtime.CompilerServices;


// El manejo de excepciones permite:
// 1. Solo crear un usuario empleado con rol y usuario admin
// 2. Verifica que el numero de panchos y hamburguesas sean enteros positivos
namespace Excepciones
{
    // Se espera numero positivo
    public class ExExpectPosInt : System.Exception 
    {
        public ExExpectPosInt() : base("Las cantidades de panchos y hamburguesas deben ser numeros enteros positivos.") { }        
    }




    // El rol 1 reservado para admin
    public class ExRolNoValido : System.Exception 
    {
        public ExRolNoValido() : base("El rol que se quiere asignar no es valido.\n El unico capas de llevar el rol 1 es el admin.") { }
    }




    // Solo se permite crear 1 usuario "admin"
    public class ExUsuarioNoPermitido : System.Exception 
    {
        public ExUsuarioNoPermitido() : base("El nombre de usuario que se quiere asignar no esta permitido.\nSolo es posible crear un admin.") { }
    }
}
